#!/usr/bin/perl
use File::Basename;
use File::Copy;
use File::Find::Rule;
########################
my $ConvertLimit=100;
my $dir='F:\developer\djvu2pdf\test';
my $PathConvert='C:\\Program Files\\DjVuZone\\DjVuLibre\\ddjvu.exe';
my $ConvertAttrib='-format=pdf -quality=100 -subsample=2';
my $OutputDir='F:\developer\djvu2pdf\output';
#########################
my $NameFile;
my $FileDir;
my $Counter=0;
my $idx_djvu='.djvu';
my $idx_pdf='.pdf';
my $RemFolder;
if (-e "mem.dat"){open FF, "+< mem.dat";} else {open FF, "+>> mem.dat";close FF;open FF, "+< mem.dat";}
opendir DIR, $dir or return;
my @contents = map "$dir/$_",sort grep !/^\.\.?$/&!/[^0-9]/,readdir DIR;
open FH,">array.log";
print (FH @contents);
close FH;
closedir DIR;


my $find_rule = File::Find::Rule->new;
$find_rule->maxdepth(1);
$find_rule->file;
$find_rule->name('*'.$idx_djvu,'*'.$idx_pdf);
my @sub_files = $find_rule->in(@contents);
 open FH,">array2.log";
 print (FH @sub_files);
 close FH;
chomp @sub_files;

my @Listing=<FF>;
chomp @Listing;

&recur();
 
sub recur {
 my $dir = shift;
 my $Flag;
 foreach (@sub_files){
	if (!-l && -d){
     recur($_);
    }
	else {
	   $Flag=0;
	   if ( $_ ~~ @Listing ) {
	     $Flag=1;
	   }
         if($Flag==0){
     if ($_ =~ /$idx_pdf$/i) {
       $FileDir=dirname($_);
       $FileDir=basename($FileDir);
       if($RemFolder!=$FileDir){$RemFolder=$FileDir;++$Counter;}
       if ($Counter<=$ConvertLimit){
       print "$Counter $_.\n";
       $NameFile=basename($_);
       mkdir "$OutputDir\\$FileDir";
       if(-e "$OutputDir\\$FileDir\\$NameFile"){$NameFile=basename($_,'.pdf');copy($_,"$OutputDir\\$FileDir\\$NameFile\_1.pdf");} else       
       {copy($_,"$OutputDir\\$FileDir");}
       print(FF "$_\n");
	   } 
       else {exit 1;}
      }
      elsif ($_ =~ /$idx_djvu$/i) {
       $FileDir=dirname($_);
       $FileDir=basename($FileDir); 
       if($RemFolder!=$FileDir){$RemFolder=$FileDir;++$Counter;}
       if ($Counter<=$ConvertLimit){       
       print "$Counter $_.\n";
       $NameFile=basename($_,'.djvu');
       mkdir "$OutputDir\\$FileDir";
       system " \"$PathConvert\" $ConvertAttrib $_ $OutputDir\\$FileDir\\$NameFile.pdf";
      if(-e "$OutputDir\\$FileDir\\$NameFile.pdf") {print(FF "$_\n")} else
       {system " \"$PathConvert\" $ConvertAttrib $_ $OutputDir\\$FileDir\\$NameFile.pdf";}
	   }
	   else {exit 1;}
       }
     else { next; }
     }
    }
  }
}
close FF;
exit 1;